from pydantic import BaseModel
from app.offers.schemas import OfferCreate, OfferResponse

class ProductCreate(BaseModel):
    name: str
    description: str
    image: str
    
class ProductResponse(BaseModel):
    id: int
    name: str
    description: str
    image: str
