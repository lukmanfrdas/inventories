from sqlalchemy import Column, Float, Integer, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from datetime import datetime
from app.database import Base

class ReviewRating(Base):
    __tablename__ = "review_ratings"

    id = Column(Integer, primary_key=True, index=True)
    review_id = Column(Integer, ForeignKey("reviews.id"))
    rating = Column(Float,index=True)
    rating_value = Column(Float,index=True)
    best_rating = Column(Float,index=True)
    worst_rating = Column(Float,index=True)
    created_at = Column(DateTime, default=datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    deleted_at = Column(DateTime, nullable=True)
    
    review = relationship("Review", back_populates="ratings")