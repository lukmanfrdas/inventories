from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from app.database import get_db
from app.review_ratings.schemas import ReviewRatingCreate, ReviewRatingResponse
from app.review_ratings.models import ReviewRating

router = APIRouter()

# Create a new review rating
@router.post("/", response_model=ReviewRatingResponse)
def create_review_rating(rating: ReviewRatingCreate, db: Session = Depends(get_db)):
    db_rating = ReviewRating(**rating.dict())
    db.add(db_rating)
    db.commit()
    db.refresh(db_rating)
    return ReviewRatingResponse(**db_rating.__dict__)

# Get a review rating by ID
@router.get("/{rating_id}", response_model=ReviewRatingResponse)
def read_review_rating(rating_id: int, db: Session = Depends(get_db)):
    rating = db.query(ReviewRating).filter(ReviewRating.id == rating_id).first()
    if rating is None:
        raise HTTPException(status_code=404, detail="Review Rating not found")
    return rating

# Update a review rating by ID
@router.put("/{rating_id}", response_model=ReviewRatingResponse)
def update_review_rating(rating_id: int, rating: ReviewRatingCreate, db: Session = Depends(get_db)):
    db_rating = db.query(ReviewRating).filter(ReviewRating.id == rating_id).first()
    if db_rating is None:
        raise HTTPException(status_code=404, detail="Review Rating not found")
    
    for key, value in rating.dict().items():
        setattr(db_rating, key, value)
    
    db.commit()
    db.refresh(db_rating)
    return ReviewRatingResponse(**db_rating.__dict__)

# Delete a review rating by ID
@router.delete("/{rating_id}", response_model=ReviewRatingResponse)
def delete_review_rating(rating_id: int, db: Session = Depends(get_db)):
    rating = db.query(ReviewRating).filter(ReviewRating.id == rating_id).first()
    if rating is None:
        raise HTTPException(status_code=404, detail="Review Rating not found")
    
    db.delete(rating)
    db.commit()
    return rating

# List all review ratings with optional pagination
@router.get("/", response_model=list[ReviewRatingResponse])
def list_review_ratings(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    ratings = db.query(ReviewRating).offset(skip).limit(limit).all()
    return ratings
