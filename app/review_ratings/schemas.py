from pydantic import BaseModel

class ReviewRatingCreate(BaseModel):
    rating: int
    rating_value: float
    best_rating: float
    worst_rating: float

class ReviewRatingResponse(BaseModel):
    id: int
    review_id: int
    rating: float
    rating_value: float
    best_rating: float
    worst_rating: float