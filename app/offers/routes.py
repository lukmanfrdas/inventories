from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from app.database import get_db
from app.offers.schemas import OfferCreate, OfferResponse
from app.offers.models import Offer

router = APIRouter()

# Create a new offer
@router.post("/", response_model=OfferResponse)
def create_offer(offer: OfferCreate, db: Session = Depends(get_db)):
    db_offer = Offer(**offer.dict())
    db.add(db_offer)
    db.commit()
    db.refresh(db_offer)
    return OfferResponse(**db_offer.__dict__)

# Get an offer by ID
@router.get("/{offer_id}", response_model=OfferResponse)
def read_offer(offer_id: int, db: Session = Depends(get_db)):
    offer = db.query(Offer).filter(Offer.id == offer_id).first()
    if offer is None:
        raise HTTPException(status_code=404, detail="Offer not found")
    return offer

# Update an offer by ID
@router.put("/{offer_id}", response_model=OfferResponse)
def update_offer(offer_id: int, offer: OfferCreate, db: Session = Depends(get_db)):
    db_offer = db.query(Offer).filter(Offer.id == offer_id).first()
    if db_offer is None:
        raise HTTPException(status_code=404, detail="Offer not found")
    
    for key, value in offer.dict().items():
        setattr(db_offer, key, value)
    
    db.commit()
    db.refresh(db_offer)
    return OfferResponse(**db_offer.__dict__)

# Delete an offer by ID
@router.delete("/{offer_id}", response_model=OfferResponse)
def delete_offer(offer_id: int, db: Session = Depends(get_db)):
    offer = db.query(Offer).filter(Offer.id == offer_id).first()
    if offer is None:
        raise HTTPException(status_code=404, detail="Offer not found")
    
    db.delete(offer)
    db.commit()
    return offer

# List all offers with optional pagination
@router.get("/", response_model=list[OfferResponse])
def list_offers(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    offers = db.query(Offer).offset(skip).limit(limit).all()
    return offers
