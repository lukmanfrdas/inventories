from pydantic import BaseModel

class OfferCreate(BaseModel):
    availability: int
    price: float
    price_currency: str

class OfferResponse(BaseModel):
    id: int
    product_id: int
    availability: int
    price: float
    price_currency: str