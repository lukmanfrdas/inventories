from pydantic import BaseModel
from app.review_ratings.schemas import ReviewRatingCreate, ReviewRatingResponse

class ReviewCreate(BaseModel):
    author: str
    date_publish: str
    review_body: str
    name: str
    reviewRating: ReviewRatingCreate


class ReviewResponse(BaseModel):
    id: int
    product_id: int
    author: str
    date_publish: str
    review_body: str
    name: str
    reviewRating: ReviewRatingResponse