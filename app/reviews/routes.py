from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session
from app.database import get_db
from app.reviews.schemas import ReviewCreate, ReviewResponse
from app.reviews.models import Review

router = APIRouter()

# Create a new review
@router.post("/", response_model=ReviewResponse)
def create_review(review: ReviewCreate, db: Session = Depends(get_db)):
    db_review = Review(**review.dict())
    db.add(db_review)
    db.commit()
    db.refresh(db_review)
    return ReviewResponse(**db_review.__dict__)

# Get a review by ID
@router.get("/{review_id}", response_model=ReviewResponse)
def read_review(review_id: int, db: Session = Depends(get_db)):
    review = db.query(Review).filter(Review.id == review_id).first()
    if review is None:
        raise HTTPException(status_code=404, detail="Review not found")
    return review

# Update a review by ID
@router.put("/{review_id}", response_model=ReviewResponse)
def update_review(review_id: int, review: ReviewCreate, db: Session = Depends(get_db)):
    db_review = db.query(Review).filter(Review.id == review_id).first()
    if db_review is None:
        raise HTTPException(status_code=404, detail="Review not found")
    
    for key, value in review.dict().items():
        setattr(db_review, key, value)
    
    db.commit()
    db.refresh(db_review)
    return ReviewResponse(**db_review.__dict__)

# Delete a review by ID
@router.delete("/{review_id}", response_model=ReviewResponse)
def delete_review(review_id: int, db: Session = Depends(get_db)):
    review = db.query(Review).filter(Review.id == review_id).first()
    if review is None:
        raise HTTPException(status_code=404, detail="Review not found")
    
    db.delete(review)
    db.commit()
    return review

# List all reviews with optional pagination
@router.get("/", response_model=list[ReviewResponse])
def list_reviews(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    reviews = db.query(Review).offset(skip).limit(limit).all()
    return reviews