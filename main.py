from fastapi import FastAPI
from uvicorn import run
from app.products.routes import router as products_router
from app.offers.routes import router as offers_router
from app.reviews.routes import router as reviews_router
from app.review_ratings.routes import router as review_ratings_router

app = FastAPI()

app.include_router(products_router, prefix="/products", tags=["Products"])
app.include_router(offers_router, prefix="/offers", tags=["Offers"])
app.include_router(reviews_router, prefix="/reviews", tags=["Reviews"])
app.include_router(review_ratings_router, prefix="/review-ratings", tags=["Review Ratings"])

if __name__ == "__main__":
    run(app, host="0.0.0.0", port=8000)
